import {
  IonList,
  IonItem,
  IonIcon,
} from '@ionic/react';
import { Link } from 'react-router-dom';
import Locales from '../i18n/locales.js';
import Tag from '../components/Tag';

import './Musiccard.css';

function Musiccard(props)
{
    let image = "https://partiturak.eus/"+props.musicsheet.mainPdf.replace(/\/(.*).pdf/,'/$1.jpg');
    return (
                <div className="card">
                    <Link className="menuLink" to={"/view/"+props.musicsheet.id}>
                    <h2>{ props.musicsheet.name }</h2>

                    <div className="image">
                        <img src={image} alt="" />
                    </div>
                    </Link>
                    <div className="tags">
                    {
                        props.musicsheet.tags.map((tagId) => {
                            let  tag = props.data.tags[tagId];
                            tag.id = tagId;
                            if(!tag.isInstrument && !tag.isOrigin) {
                                return (<Tag tag={tag} />);
                            }
                        })
                    }
                    </div>
                </div>

  );
};

export default Musiccard;
