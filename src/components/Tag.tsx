import {
  IonList,
  IonBadge,
  IonIcon,
} from '@ionic/react';
import { Link } from 'react-router-dom';
import Locales from '../i18n/locales.js';
import './Tag.css';

function Tag(props)
{
    let link = '';
    console.log('tag is ',props.tag);
    if(props.tag.isType) { link = '/category/'+props.tag.id; }
    if(props.tag.isAuthor) { link = '/author/'+props.tag.id; }
    if(props.tag.isInstrument) { link = '/instrument/'+props.tag.id; }

    return (
    <Link to={link}>
         <IonBadge className="tag">
            { props.tag.name }
        </IonBadge>
    </Link>
  );
};

export default Tag;
