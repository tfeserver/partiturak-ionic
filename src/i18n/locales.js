export default
{
    loading:"Kargatzen...",
    appName: "Partiturak.eus",
    menu: "Partiturak.eus",
    home: "Hasiera",
    categories: "Kategoriak",
    authors: "Autoreak",
    instruments: "Musika tresnak",
    home:
    {
        menu: "Hasiera",
        title: "Asken partiturak"
    }

}
