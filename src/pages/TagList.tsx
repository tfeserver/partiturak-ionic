import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import { 
  IonButton,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonMenu,
  IonMenuToggle,
  IonPage,
  IonRadio,
  IonRadioGroup,
  IonTitle,
  IonToolbar
} from '@ionic/react';
import type { RadioGroupCustomEvent } from '@ionic/react';
import Locales from '../i18n/locales.js';
import Menu from '../components/Menu';
import Header from '../components/Header';
import Musicsheets from '../components/Musicsheets';

function TagList(props) {

    let params = useParams();
    let newProps = { ... props };
    newProps.pageId = props.type+'-'+params.tagId+'-content';

    let tag = props.data.tags[ params.tagId ];

    let musicsheets= [];
    let indexes = props.data.musicsheetsByTags[params.tagId];
    if(indexes)
    {
        indexes.forEach((key) => {
            props.data.musicsheets[key].id = key;
            musicsheets.push( props.data.musicsheets[key]);
        });
    }

    console.log('tag is ',tag);
    return (
            <>
            <IonPage id="main-content">
            <Header />
            <IonContent className="ion-padding">
            <h2>Tag { tag.name }</h2>
            <Musicsheets { ... props } currentsheets={musicsheets} />

            </IonContent>
            </IonPage>
            </>
           );
}
export default TagList;
