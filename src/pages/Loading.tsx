import React, { useState } from 'react';
import { 
    IonPage,
    IonContent,
  IonLoading
} from '@ionic/react';
import type { RadioGroupCustomEvent } from '@ionic/react';
import Locales from '../i18n/locales.js';
import Header from '../components/Header';
import Musicsheets from '../components/Musicsheets';
import { useParams } from 'react-router-dom';

function Loading() {

    return (
            <>
            <IonPage id="main-content">
                <Header />

                <IonContent className="ion-padding">
                    <IonLoading isOpen={true} spinner="circles"  message={Locales.loading} />
                </IonContent>
            </IonPage>
            </>
           );
}
export default Loading;
