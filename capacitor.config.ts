import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'partiturak',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;
